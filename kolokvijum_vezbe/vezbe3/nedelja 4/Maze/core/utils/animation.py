from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets


class Animation(QtWidgets.QGraphicsItemGroup):
    def __init__(self, animation_path):
        super().__init__()
        self.animation_path = animation_path
        self.character = QtWidgets.QGraphicsPixmapItem(QtGui.QPixmap("resources/characters/robot.png"))
        self.character.setPos(animation_path[0][0].x*64, animation_path[0][0].y*64)
        self.direction_markers = QtWidgets.QGraphicsItemGroup()
        self.items = []

        self.addToGroup(self.direction_markers)
        self.addToGroup(self.character)

        self._animation_timer = QtCore.QTimer()

        self._animation_timer.timeout.connect(self._update_progress)

    def run_animation(self):
        self._animation_timer.start(250)

    def stop_animation(self):
        if self._animation_timer.isActive():
            self._animation_timer.stop()

    def _create_direction_marker(self, path_item, color_scheme=["#ff9f00", "#00ff00"]):
        if path_item.parent_state is not None:
            if path_item.parent_state.x < path_item.x:
                marker = QtWidgets.QGraphicsPolygonItem([QtCore.QPointF(path_item.x*64, path_item.y*64+10),
                                                        QtCore.QPointF(path_item.x*64+22, path_item.y*64+32),
                                                        QtCore.QPointF(path_item.x*64, path_item.y*64+54)])
            if path_item.parent_state.x > path_item.x:
                marker = QtWidgets.QGraphicsPolygonItem([QtCore.QPointF(path_item.x*64+64, path_item.y*64+10),
                                                        QtCore.QPointF(path_item.x*64+42, path_item.y*64+32),
                                                        QtCore.QPointF(path_item.x*64+64, path_item.y*64+54)])
            if path_item.parent_state.y < path_item.y:
                marker = QtWidgets.QGraphicsPolygonItem([QtCore.QPointF(path_item.x*64+10, path_item.y*64),
                                                        QtCore.QPointF(path_item.x*64+32, path_item.y*64+22),
                                                        QtCore.QPointF(path_item.x*64+54, path_item.y*64)])
            if path_item.parent_state.y > path_item.y:
                marker = QtWidgets.QGraphicsPolygonItem([QtCore.QPointF(path_item.x*64+10, path_item.y*64+64),
                                                        QtCore.QPointF(path_item.x*64+32, path_item.y*64+42),
                                                        QtCore.QPointF(path_item.x*64+54, path_item.y*64+64)])
            marker.setPen(QtCore.Qt.NoPen)
            marker.setBrush(QtGui.QBrush(QtGui.QColor(color_scheme[0])))
            marker.setOpacity(0.1)
        else:
            marker = QtWidgets.QGraphicsEllipseItem(path_item.x*64, path_item.y*64, 64, 64)
            marker.setPen(QtCore.Qt.NoPen)
            marker.setBrush(QtGui.QBrush(QtGui.QColor(color_scheme[1])))
            marker.setOpacity(0.1)
        return marker

    def _update_progress(self):
        if len(self.animation_path[1]) > 0:
            path_item = self.animation_path[1].pop(0)
            direction_marker = self._create_direction_marker(path_item)
            self.items.append(direction_marker)
            self.direction_markers.addToGroup(direction_marker)
            self.character.setPos(path_item.x*64, path_item.y*64)
        elif len(self.animation_path[0]) > 0:
            path_item = self.animation_path[0].pop(0)
            direction_marker = self._create_direction_marker(path_item, ["#00ff00", "#00ff00"])
            self.items.append(direction_marker)
            self.direction_markers.addToGroup(direction_marker)
            self.character.setPos(path_item.x*64, path_item.y*64)
        else:
            self._animation_timer.stop()
