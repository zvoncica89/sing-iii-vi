from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2 import QtGui
from .model.maze_model import MazeModel
from .widgets.maze_editor import MazeEditor

from .solver.solver import Solver
from .solver.algorithms.dfs import dfs
from .solver.algorithms.bfs import bfs
from .solver.algorithms.dijkstras import dijkstras
from .solver.algorithms.astar import astar
from .solver.algorithms.moj_algoritam import moj_algoritam

from .utils.animation import Animation


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title=""):
        super().__init__()
        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon("resources/icons/brain.png"))
        self.resize(800, 600)

        self.actions = {
            "run": QtWidgets.QAction(QtGui.QIcon("resources/icons/control.png"), "Run"),
            "solve": QtWidgets.QAction(QtGui.QIcon("resources/icons/control-double.png"), "Run")
        }

        self.central_widget = MazeEditor(self, MazeModel.read("mazes/maze4.json"))

        self.create_toolbar()

        self.setCentralWidget(self.central_widget)

        self.tile_palette_dock = QtWidgets.QDockWidget()
        self.tile_palette_dock.setWindowTitle("Tile palette")
        self.tile_palette_dock.setWidget(self.central_widget.tiles_palette)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.tile_palette_dock)

        self._bind_actions()

    def create_toolbar(self):
        self.toolbar = QtWidgets.QToolBar("Toolbar")
        self.search_type_select = QtWidgets.QComboBox()
        self.search_type_select.addItem("Moj algoritam", moj_algoritam)
        self.search_type_select.addItem("Depth first search", dfs)
        self.search_type_select.addItem("Breadth first search", bfs)
        self.search_type_select.addItem("Dijkstra's algorithm", dijkstras)
        self.search_type_select.addItem("A*", astar)
        

        self.addToolBar(self.toolbar)

        self.toolbar.addAction(self.central_widget.actions["new"])
        self.toolbar.addAction(self.central_widget.actions["open"])
        self.toolbar.addAction(self.central_widget.actions["save"])
        self.toolbar.addSeparator()

        self.toolbar.addWidget(self.search_type_select)

        self.toolbar.addAction(self.actions["run"])
        self.toolbar.addAction(self.actions["solve"])

    def run_solver(self, option=True):
        solver = Solver(self.central_widget.model, self.search_type_select.currentData())
        result = solver.solve(option)

        self.central_widget.set_animation(Animation(result))
        self.central_widget.run_animation()
        

    def _bind_actions(self):
        self.actions["run"].triggered.connect(lambda : self.run_solver(True))
        self.actions["solve"].triggered.connect(lambda : self.run_solver(False))
