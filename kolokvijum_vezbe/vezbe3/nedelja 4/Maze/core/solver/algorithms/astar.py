def astar(start_state, maze, goal, ignore_states = []):
    poseceni = []
    neposeceni = [start_state]

    while len(neposeceni)>0:
        neposeceni.sort(key = lambda s: s.total_cost)
        na_obradi = neposeceni.pop(0)

        if na_obradi.tile.get(goal) and na_obradi not in ignore_states:
            return(na_obradi,poseceni)
        
        if na_obradi not in poseceni:
            poseceni.append(na_obradi)
            sledeca_stanja = na_obradi.get_next_states(maze)
            for sledece_stanje in sledeca_stanja:
                if sledece_stanje not in poseceni and sledece_stanje not in neposeceni:
                    neposeceni.append(sledece_stanje)
                elif sledece_stanje in neposeceni:
                    if sledece_stanje.total_cost < neposeceni[neposeceni.index(sledece_stanje)].total_cost:
                        neposeceni[neposeceni.index(sledece_stanje)].total_cost = sledece_stanje.total_cost

    return (None, poseceni)