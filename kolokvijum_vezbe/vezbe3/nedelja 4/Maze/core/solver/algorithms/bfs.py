def bfs(start_state, maze, goal):
    poseceni = []
    neposeceni = []

    neposeceni.append(start_state)
    while len(neposeceni)>0:
        na_obradi = neposeceni.pop(0)

        if na_obradi.tile.get(goal):
            return(na_obradi,poseceni)
        
        if na_obradi not in poseceni:
            poseceni.append(na_obradi)
            sledeca_stanja = na_obradi.get_next_states(maze)
            for sledeca_stanje in sledeca_stanja:
                if sledeca_stanje not in poseceni:
                    neposeceni.append(sledeca_stanje)

    return (None, poseceni)

