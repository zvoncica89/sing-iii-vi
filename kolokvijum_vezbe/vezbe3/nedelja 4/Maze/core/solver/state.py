import math
import random

class State:
    def __init__(self, x, y, tile, parent_state=None):
        self.x = x
        self.y = y
        self.tile = tile
        self.parent_state = parent_state
        self.cost = tile.get("cost",1) #isto sto i dole samo sto ovo ako je none uzima ovu drugu vrednost
        # if tile.get("cost") is not None:
        #     self.cost += tile.get("cost")
        if parent_state is not None:
            self.cost += parent_state.cost
        
        self.total_cost = self.cost
        self.keys_found = False

    def hstar(self, maze_model):
        goal_tiles = maze_model.get_tiles_by_type("exit")
        min_cost = (self.x - goal_tiles[0]["x"])**2 + (self.y - goal_tiles[0]["y"])**2
        for goal_tile in goal_tiles:
            potencial_cost = (self.x - goal_tile["x"])**2 + (self.y - goal_tile["y"])**2
            if potencial_cost < min_cost:
                min_cost = potencial_cost
        self.total_cost += min_cost

    def heuristic(self,maze_model):
        goal_tiles = maze_model.get_tiles_by_type("fire")
        sigma = 1
        coef = 1/(2*math.pi*sigma**2)
        cost = 0
        for fire in goal_tiles:
            ux = fire["x"]
            uy = fire["y"]
            cost += 1000*coef*math.exp(-((self.x-ux)**2+(self.y-uy)**2)/(2*sigma**2))
        return cost
        

    def h(self, maze_model):
        tiles = maze_model.get_tiles_by_type("exit")
        tile = min(tiles, key=lambda t: abs(t["x"]-self.x) + abs(t["y"]-self.y))
        return abs(tile["x"]-self.x) + abs(tile["y"]-self.y)

    def teleport(self, maze_model):
        next_state = None
        while next_state == None:
            number = random.randint(0,9)
            print(number)
            nx = random.randint(0,maze_model.size_x-1)
            ny = random.randint(0, maze_model.size_y-1)
            if number < 7:
                tile = maze_model.get_tiles_by_type("exit")[0]
                goal = State(tile["x"], tile["y"], maze_model.get_tile(tile["x"],tile["y"]), self)
                if maze_model.get_tile(goal.x-(nx//2),goal.y-(ny//2))["passable"]==True:
                    next_state = State(goal.x-nx, goal.y-ny, maze_model.get_tile(goal.x-nx,goal.y-ny), self)
                    return next_state
                else:
                    next_state = None
            elif number > 6 and number < 9:                
                if maze_model.get_tile(nx, ny)["passable"]==True:
                    next_state = State(nx, ny, maze_model.get_tile(nx, ny), self)
                    return next_state
                else:
                    next_state = None
            elif number == 9: 
                fires = maze_model.get_tiles_by_type("fire")[0]
                next_state = State(fires["x"], fires["y"], maze_model.get_tile(fires["x"],fires["y"]), self)
                return next_state





    def get_next_states(self, maze_model):
        next_states = []

    #proverava se prvo da li ispada iz okvira pa onda da li moze da se pomeri
        if self.x+1<maze_model.size_x and maze_model.get_tile(self.x+1, self.y)["passable"]==True:
            desno = State(self.x+1,self.y,maze_model.get_tile(self.x+1, self.y), self)
            if maze_model.get_tile(self.x+1, self.y)["title"]=="Teleport":
                next_state = self.teleport(maze_model)
                next_states.append(next_state)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)
            else:
                next_states.append(desno)
                # desno.total_cost += desno.heuristic(maze_model)
                
                # self.total_cost = desno.total_cost +desno.heuristic(maze_model)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)

        if self.x-1>=0 and maze_model.get_tile(self.x-1, self.y)["passable"]==True:
            levo = State(self.x-1,self.y,maze_model.get_tile(self.x-1, self.y),self)
            if maze_model.get_tile(self.x-1, self.y)["title"]=="Teleport":
                next_state = self.teleport(maze_model)
                next_states.append(next_state)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)
            else:
                next_states.append(levo)
                # levo.total_cost += levo.heuristic(maze_model)
                # self.total_cost = levo.total_cost +levo.heuristic(maze_model)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)

        if self.y-1>=0 and maze_model.get_tile(self.x,self.y-1)["passable"]==True:
            gore = State(self.x, self.y-1, maze_model.get_tile(self.x,self.y-1),self)
            if maze_model.get_tile(self.x, self.y-1)["title"]=="Teleport":
                next_state = self.teleport(maze_model)
                next_states.append(next_state)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)
            else:
                next_states.append(gore)
                # gore.total_cost += gore.heuristic(maze_model)
                # self.total_cost = gore.total_cost +gore.heuristic(maze_model)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)

        if self.y+1<maze_model.size_y and maze_model.get_tile(self.x, self.y+1)["passable"]==True:
            dole = State(self.x,self.y+1, maze_model.get_tile(self.x, self.y+1), self)
            if maze_model.get_tile(self.x, self.y+1)["title"]=="Teleport":
                next_state = self.teleport(maze_model)
                next_states.append(next_state)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)
            else:
                next_states.append(dole)
                # dole.total_cost += dole.heuristic(maze_model)
                # self.total_cost = dole.total_cost +dole.heuristic(maze_model)
                next_states[-1].total_cost += next_states[-1].heuristic(maze_model)

        # print("Cost:", self.total_cost)
        return next_states

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))


