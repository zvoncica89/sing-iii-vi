from .dijkstras import dijkstras
def moj_algoritam(start_state, maze, goal):
    broj_kljuceva = 2
    lista_ignore = []
    poseceni = []
    for i in range(0, broj_kljuceva):
        kljuc,poseceni_do_sledeceg_stanja = dijkstras(start_state,maze,"key",lista_ignore)
        start_state = kljuc
        lista_ignore.append(kljuc)
        poseceni.extend(poseceni_do_sledeceg_stanja)
    izlaz, poseceni_do_izlaza = dijkstras(start_state,maze,goal, lista_ignore)
    # kljuc2, poseceni_do_drugog_kljuca = dijkstras(kljuc,maze,"key", lista_ignore)
    # lista_ignore.append(kljuc2)
    # poseceni.extend(poseceni_do_drugog_kljuca)
    # izlaz, poseceni_do_izlaza = dijkstras(kljuc2,maze,goal, lista_ignore)
    poseceni.extend(poseceni_do_izlaza)
    return (izlaz, poseceni)