def dijkstras(start_state, maze, goal, ignore = []):
    poseceni = []
    neposeceni = []

    neposeceni.append(start_state)
    while len(neposeceni)>0:
        neposeceni.sort(key = lambda s: s.cost)
        na_obradi = neposeceni.pop(0)

        if na_obradi.tile.get(goal) and na_obradi not in ignore:
            return(na_obradi,poseceni)
        
        if na_obradi not in poseceni:
            poseceni.append(na_obradi)
            sledeca_stanja = na_obradi.get_next_states(maze)
            for sledece_stanje in sledeca_stanja:
                if sledece_stanje not in poseceni:
                    neposeceni.append(sledece_stanje)
                else:
                    if sledece_stanje.cost < poseceni[poseceni.index(sledece_stanje)].cost:
                        neposeceni.append(sledece_stanje)

    return (None, poseceni)