class State:
    def __init__(self, x, y, tile, parent_state=None):
        self.x = x
        self.y = y
        self.tile = tile
        self.parent_state = parent_state
        self.cost = tile.get("cost",1) #isto sto i dole samo sto ovo ako je none uzima ovu drugu vrednost
        # if tile.get("cost") is not None:
        #     self.cost = tile.get("cost")
        if parent_state is not None:
            self.cost += parent_state.cost
        self.hstar = self.cost

    def heuristic(self,maze_model):
        # goal_tiles = maze_model.get_tiles_by_type("exit")
        # closest_goal = (self.x - goal_tiles[0].get("x"))**2+(self.y - goal_tiles[0].get("y"))**2
        # for goal_tile in goal_tiles:
        #     if (self.x - goal_tile.get("x"))**2+(self.y - goal_tile.get("y"))**2 < closest_goal:
        #         closest_goal = (self.x - goal_tile.get("x"))**2+(self.y - goal_tile.get("y"))**2
        # return closest_goal
        tiles = maze_model.get_tiles_by_type("exit")
        tile = min(tiles, key= lambda t: abs(t["x"] - self.x ) + abs(t["y"] - self.y))
        return abs(tile["x"]-self.x) + abs(tile["y"] - self.y)
        

    def get_next_states(self, maze_model):
        next_states = []

    #proverava se prvo da li ispada iz okvira pa onda da li moze da se pomeri
        if self.x+1<maze_model.size_x and maze_model.get_tile(self.x+1, self.y)["passable"]==True:
            desno = State(self.x+1,self.y,maze_model.get_tile(self.x+1, self.y), self)
            next_states.append(desno)
            desno.hstar += desno.heuristic(maze_model)

        if self.x-1>=0 and maze_model.get_tile(self.x-1, self.y)["passable"]==True:
            levo = State(self.x-1,self.y,maze_model.get_tile(self.x-1, self.y),self)
            next_states.append(levo)
            levo.hstar += levo.heuristic(maze_model)

        if self.y-1>=0 and maze_model.get_tile(self.x,self.y-1)["passable"]==True:
            gore = State(self.x, self.y-1, maze_model.get_tile(self.x,self.y-1),self)
            next_states.append(gore)
            gore.hstar += gore.heuristic(maze_model)

        if self.y<maze_model.size_y and maze_model.get_tile(self.x, self.y+1)["passable"]==True:
            dole = State(self.x,self.y+1, maze_model.get_tile(self.x, self.y+1), self)
            next_states.append(dole)
            dole.hstar += dole.heuristic(maze_model)


        return next_states

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))


